package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling {

	final int Turno = 20;
	final int Partita = 10;
	private int score = 0;
	private int contatore = 0;
	private int punteggio_turno = 0;

	@Override
	public void roll(int pins) {
		if (isSpare(punteggio_turno))
			score += pins * 2;
		else {
			if (contatore % 2 == 0)
				punteggio_turno = 0;
			punteggio_turno += pins;
			score += pins;
			contatore++;
		}
	}

	public boolean isSpare(int punteggio) {
		return Partita == punteggio;
	}

	@Override
	public int score() {
		return score;
	}

}
